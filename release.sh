#!/bin/bash
## GITHUB_ACTIONS
## GITHUB_EVENT_NAME
## GITHUB_ACTOR
##
## GITHUB_REPOSITORY
## GITHUB_SHA
## GITHUB_REF
##
## RUNNER_TEMP
## RUNNER_OS
##
## https://rabbit.octopus.app/app#/Spaces-42/projects/docker-site/overview
## https://developer.github.com/v3/repos/releases/#create-a-release


# export _RELEASE_VERSION=$(node -e 'console.log(require("./templates/package").version)';)
export _RELEASE_VERSION=0.5.0
# export _PATCH_VERSION="$(date +"%H%M")"
export _BASE_NAME="$(basename $PWD)"
export _RELEASE_NAME="${_BASE_NAME}.${_RELEASE_VERSION}"
export _RELEASE_TYPE=${1};

#echo "Creating [$_RELEASE_NAME] in v[${_RELEASE_VERSION}], will use [$_PATCH_VERSION] patch version for release."

## GitHub Tag and Release
##
# function githubRelease {

#   ## create tag
#   curl -s -XPOST -d '{"ref": "refs/tags/'${_RELEASE_VERSION}'","sha": "'${GITHUB_SHA}'"}' --header "Content-Type:application/json" --header 'Accept:application/vnd.github.mercy-preview+json' --header "Authorization: token  ${GITHUB_TOKEN}" https://api.github.com/repos/${GITHUB_REPOSITORY}/git/refs

#   ## create release
#   curl -s -XPOST -d '{"name":"'${_RELEASE_VERSION}'","target_commitish":"latest","tag_name":"'${_RELEASE_VERSION}'","body":"","draft":false ,"prerelease":false}' --header "Content-Type:application/json" --header 'Accept:application/vnd.github.mercy-preview+json' --header "Authorization: token  ${GITHUB_TOKEN}" https://api.github.com/repos/${GITHUB_REPOSITORY}/releases

# }

## Octopus Package and Release
##
function octopusRelease {

  echo $_RELEASE_VERSION
  echo $_BASE_NAME
  echo $_RELEASE_NAME
  echo $_RELEASE_TYPE

  tar --exclude '.DS_Store' -czvf "$_RELEASE_NAME.tar.gz" -C . .

  ## Upload Release Package
  curl -s -XPOST "https://rabbit.octopus.app/api/Spaces-42/packages/raw?apiKey=${OCTOPUS_CLI_API_KEY}&replace=true" -F "data=@$_RELEASE_NAME.tar.gz"

  ## Create Release
  curl -s -XPOST "https://rabbit.octopus.app/api/Spaces-42/releases" -H "X-Octopus-ApiKey:${OCTOPUS_CLI_API_KEY}" -d '{
    "ProjectId":"Projects-261",
    "ChannelId": "Channels-261",
    "Version":"'${_RELEASE_VERSION}'",
    "ReleaseNotes": "test"
  }'

  rm -rf "$_RELEASE_NAME.tar.gz" ;

}

if [[ "$_RELEASE_TYPE" == "github" ]]; then
  githubRelease
fi;

if [[ "$_RELEASE_TYPE" == "octopus" ]]; then
  octopusRelease
fi;